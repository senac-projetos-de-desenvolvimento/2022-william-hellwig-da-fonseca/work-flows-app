import { Box, Button, NativeBaseProvider, Text, VStack } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View } from 'react-native';
import React from 'react';

import Global from './../Components/Global';

export default function Logout({ navigation }) {


    const Logout = async () => {
        Global.name = "user"
        Global.image = "/images/User.png"
        Global.position = "cargo"
        Global.token = ""
        Global.idUser = 0
        AsyncStorage.clear()
        navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }]
        })
    }
    return (
        <NativeBaseProvider>
            <View style={{
                flex: 1,
                resizeMode: "cover",
                backgroundColor: "#272E48",
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Box style={{
                    justifyContent: 'flex-start',
                    width: "90%",
                    alignItems: 'center',
                    backgroundColor: "#354787",
                    borderRadius: 10,
                }}>
                    <VStack style={{ marginTop: "3%", width: "70%", alignItems: 'center', backgroundColor: "#272E48", borderRadius: 15, marginBottom: "5%" }}>
                        <Text style={{ width: "90%", fontSize: 17, textAlign: 'center', marginVertical: "2%", color: "#fff" }}>Deseja Sair?</Text>
                    </VStack>
                    <VStack style={{ alignItems: 'center', width: "60%", marginVertical: "6%" }}>
                        <Button variant='solid' style={{ width: "90%", borderRadius: 25, backgroundColor: '#272E48' }} onPress={() => Logout()}><Text style={{ fontSize: 18, color: "#fff" }}>Sair</Text></Button>
                    </VStack>
                </Box>
            </View>
        </NativeBaseProvider>
    )
}  