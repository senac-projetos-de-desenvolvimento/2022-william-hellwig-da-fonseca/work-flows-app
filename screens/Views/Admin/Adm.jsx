import * as React from 'react';
import { View, Text } from 'react-native';
import {
    Box,
    NativeBaseProvider,
    Button,
    VStack,
    Image,
    Modal,
    FormControl,
    WarningOutlineIcon,
    Input,
    TextArea,
    Toast,
    Select,
    ScrollView,
    Divider
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

import api from '../../../services/api'
import Global from '../../Components/Global'

export default class Adm extends React.Component {
    state = {
        userImg: Global.image,
        idUser: Global.idUser,
        token: Global.token,
        data: new Array(),
        dataUser: new Array(),
        position: "",
        name: "",
        email: "",
        fone: "",
        ctps: "",
        password: "",
        jobPosition: Number,
        title: "",
        description: "",
        weight: Number,
        vagaDesc: '',
        altCargoUser: Number,
        erro: Boolean(false),
        erroMsg: "",
        baseURL: api.defaults.baseURL,
        showJob: Boolean,
        showUser: Boolean,
        showWar: Boolean,
        showVaga: Boolean,
        showAlt: Boolean
    }

    componentDidMount = async () => {
        this.getJobs()
        this.getUsers()
    }
    getUsers = async () => {
        const response = await api.get('/user')
        this.setState({
            dataUser: response.data.Users
        })

        setTimeout(() => {
            this.getUsers()
        }, 60000)
    }
    getJobs = async () => {
        const response = await api.get('/job')
        this.setState({
            data: response.data.Cargos
        })

        setTimeout(() => {
            this.getJobs()
        }, 60000)
    }
    sendJob = async () => {
        const response = await api.post('/job', {
            position: this.state.position,
            token: this.state.token
        })
        if (response.data.erro === false) {
            this.setState({
                showJob: false,
                position: "",
                erro: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            this.getJobs()
        }

        // console.log(response.data)
    }
    sendUser = async () => {
        const response = await api.post('/user', {
            token: this.state.token,
            name: this.state.name,
            email: this.state.email,
            fone: this.state.fone,
            ctps: this.state.ctps,
            password: this.state.password,
            job_position_id: this.state.jobPosition,
        })
        if (response.data.erro === false) {
            this.setState({
                showUser: false,
                name: "",
                email: "",
                fone: "",
                ctps: Number,
                password: "",
                jobPosition: Number,
                erro: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        } else {
            this.setState({
                showUser: false,
                name: "",
                email: "",
                fone: "",
                ctps: Number,
                password: "",
                jobPosition: Number,
                erro: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }

        // console.log(response.data)
    }
    sendVaga = async () => {
        const response = await api.post('/vacancie', {
            token: this.state.token,
            idUser: this.state.idUser,
            idJob: this.state.jobPosition,
            message: this.state.vagaDesc,
        })
        if (response.data.erro === false) {
            this.setState({
                showVaga: false,
                message: "",
                vagaDesc: null,
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        } else {
            this.setState({
                showVaga: false,
                message: "",
                vagaDesc: null,
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }
    sendWarning = async () => {
        const response = await api.post('/warning', {
            token: this.state.token,
            title: this.state.title,
            description: this.state.description,
            weight: this.state.weight
        })
        if (response.data.erro === false) {
            this.setState({
                showWar: false,
                title: "",
                description: "",
                weight: Number,
                erro: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        } else {
            this.setState({
                showWar: false,
                title: "",
                description: "",
                weight: Number,
                erro: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }

        // console.log(response.data)
    }
    altCargoUser = async () => {
        const response = await api.post('/userupjob', {
            token: this.state.token,
            id: this.state.altCargoUser,
            job_position_id: this.state.jobPosition,
        })
        if (response.data.erro === false) {
            this.setState({
                showAlt: false,
                jobPosition: null,
                erro: false
            })
            this.getUsers();
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        } else {
            this.setState({
                showAlt: false,
                jobPosition: null,
                erro: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }

    handlePositionChange = (position) => {
        this.setState({ position: position })
    }
    handleNameChange = (name) => {
        this.setState({ name: name })
    }
    handleEmailChange = (email) => {
        this.setState({ email: email })
    }
    handleFoneChange = (fone) => {
        this.setState({ fone: fone })
    }
    handleCtpsChange = (ctps) => {
        this.setState({ ctps: ctps })
    }
    handlePassChange = (password) => {
        this.setState({ password: password })
    }
    handleJobChange = (jobPosition) => {
        this.setState({ jobPosition: jobPosition })
    }
    handleTitleChange = (title) => {
        this.setState({ title: title })
    }
    handleDescChange = (description) => {
        this.setState({ description: description })
    }
    handleWeightChange = (weight) => {
        this.setState({ weight: weight })
    }
    handlevagaDescChange = (vagaDesc) => {
        this.setState({ vagaDesc: vagaDesc })
    }
    handleAltCargoChange = (altCargoUser) => {
        this.setState({ altCargoUser: altCargoUser })
    }

    render() {
        const { data, dataUser, idUser, erro, erroMsg, position, name, email, fone, ctps, password, jobPosition, title, description, weight, vagaDesc, altCargoUser } = this.state

        const cadJob = () => {
            if (position == "") {
                this.setState({
                    erroMsg: "Informe a Descrição do Cargo!",
                    erro: true,
                })
            } else {
                this.sendJob();
            }
        }
        const cadUser = () => {
            if (name == "" || email == "" || fone == "" || ctps == null || password == "" || jobPosition == null) {
                this.setState({
                    erroMsg: "Informar os Dados do Usuario!",
                    erro: true,
                })
            } else {
                this.sendUser();
            }
        }
        const cadWar = () => {
            if (title == "" || description == "" || weight == null) {
                this.setState({
                    erroMsg: "Informar os Dados do Aviso!",
                    erro: true,
                })
            } else {
                this.sendWarning();
            }
        }
        const cadVaga = () => {
            if (vagaDesc == "" || jobPosition == null) {
                this.setState({
                    erroMsg: "Informar os Dados Corretamente!",
                    erro: true,
                })
            } else {
                this.sendVaga();
            }
        }
        const altCargo = () => {
            if (altCargoUser == null || jobPosition == null) {
                this.setState({
                    erroMsg: "Informar os Dados Corretamente!",
                    erro: true,
                })
            } else {
                if (altCargoUser === idUser) {
                    this.setState({
                        erroMsg: "Você não pode alterar seu cargo!",
                        erro: true,
                    })
                } else {
                    this.altCargoUser();
                }
            }
        }
        return (
            <NativeBaseProvider>
                {/* Modal Cargos */}
                <Modal
                    isOpen={this.state.showJob}
                    onClose={() => this.setState({ showJob: false })}
                    size="xl"
                    style={{ backgroundColor: "#272E48" }}

                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Header>Cadastro de Cargos</Modal.Header>
                        <Modal.Body>
                            <FormControl isInvalid={erro}>
                                <FormControl.Label>Descrição</FormControl.Label>
                                <Input require variant="underlined" value={this.state.position} onChangeText={this.handlePositionChange} />
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", height: "28%", margin: "5%", textAlign: "center" }} onPress={() => cadJob()}>
                                <Ionicons name='checkmark-outline' size={25} color={"#fff"} /></Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Usuario */}
                <Modal
                    isOpen={this.state.showUser}
                    onClose={() => this.setState({ showUser: false })}
                    size="full"
                    style={{ backgroundColor: "#272E48" }}
                >

                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Header>Cadastro De Colaboradores</Modal.Header>
                        <Modal.Body style={{ justifyContent: 'center', alignItems: "center" }}>
                            <FormControl isInvalid={erro}>
                                <FormControl.Label>Nome</FormControl.Label>
                                <Input variant="underlined" value={this.state.name} onChangeText={this.handleNameChange} />

                                <FormControl.Label>Email</FormControl.Label>
                                <Input require variant="underlined" value={this.state.email} onChangeText={this.handleEmailChange} />

                                <FormControl.Label>Contato</FormControl.Label>
                                <Input variant="underlined" value={this.state.fone} onChangeText={this.handleFoneChange} />

                                <FormControl.Label>CTPS</FormControl.Label>
                                <Input require variant="underlined" value={this.state.ctps} onChangeText={this.handleCtpsChange} />

                                <FormControl.Label>Senha Temporaria</FormControl.Label>
                                <Input type='password' variant="underlined" value={this.state.password} onChangeText={this.handlePassChange} />

                                <FormControl.Label>Cargo</FormControl.Label>
                                <Select require onValueChange={this.handleJobChange}>
                                    {data.map((value, index) => (
                                        <Select.Item key={index} label={value.position} value={value.id} />
                                    ))}
                                </Select>
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", height: "12%", margin: "5%", textAlign: "center" }} onPress={() => cadUser()}>
                                <Ionicons name='checkmark-outline' size={25} color={"#fff"} /></Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Avisos */}
                <Modal
                    isOpen={this.state.showWar}
                    onClose={() => this.setState({ showWar: false })}
                    size="xl"
                    style={{ backgroundColor: "#272E48" }}

                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Header>Adicionar Avisos</Modal.Header>
                        <Modal.Body>
                            <FormControl isInvalid={erro}>
                                <FormControl.Label>Titulo</FormControl.Label>
                                <Input require variant="underlined" value={this.state.title} onChangeText={this.handleTitleChange} />
                                <FormControl.Label>Descrição</FormControl.Label>
                                <TextArea require variant="underlined" value={this.state.description} onChangeText={this.handleDescChange} />
                                <FormControl.Label>Tipo</FormControl.Label>
                                <Select require onValueChange={this.handleWeightChange}>
                                    <Select.Item label="Aviso Importante" value={1} />
                                    <Select.Item label="Alerta" value={2} />
                                    <Select.Item label="Informação" value={3} />
                                </Select>
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", height: "12%", margin: "5%", textAlign: "center" }} onPress={() => cadWar()}>
                                <Ionicons name='checkmark-outline' size={25} color={"#fff"} /></Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Vagas */}
                <Modal
                    isOpen={this.state.showVaga}
                    onClose={() => this.setState({ showVaga: false })}
                    size="full"
                    style={{ backgroundColor: "#272E48" }}
                >

                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Header>Cadastro De Vagas Disponiveis</Modal.Header>
                        <Modal.Body style={{ justifyContent: 'center', alignItems: "center" }}>
                            <FormControl isInvalid={erro}>
                                <FormControl.Label>Descrição</FormControl.Label>
                                <Input variant="underlined" value={this.state.vagaDesc} onChangeText={this.handlevagaDescChange} />
                                <FormControl.Label>Cargo</FormControl.Label>
                                <Select require onValueChange={this.handleJobChange}>
                                    {data.map((value, index) => (
                                        <Select.Item key={index} label={value.position} value={value.id} />
                                    ))}
                                </Select>
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", margin: "5%", textAlign: "center" }} onPress={() => cadVaga()}>
                                <Ionicons name='checkmark-outline' size={25} color={"#fff"} /></Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Alt Cargos */}
                <Modal
                    isOpen={this.state.showAlt}
                    onClose={() => this.setState({ showAlt: false })}
                    size="full"
                    style={{ backgroundColor: "#272E48" }}
                >

                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Header>Alterar Cargo Colaborador</Modal.Header>
                        <Modal.Body style={{ justifyContent: 'center', alignItems: "center" }}>
                            <FormControl isInvalid={erro}>
                                <FormControl.Label>Selecione o Colaborador</FormControl.Label>
                                <Select require onValueChange={this.handleAltCargoChange}>
                                    {dataUser.map((value, index) => (
                                        <Select.Item key={index} label={`${value.name} - ${value.position}`} value={value.id} />
                                    ))}
                                </Select>
                                <FormControl.Label>Novo Cargo</FormControl.Label>
                                <Select require onValueChange={this.handleJobChange}>
                                    {data.map((value, index) => (
                                        <Select.Item key={index} label={value.position} value={value.id} />
                                    ))}
                                </Select>
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", margin: "5%", textAlign: "center" }} onPress={() => altCargo()}>
                                <Ionicons name='checkmark-outline' size={25} color={"#fff"} /></Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    backgroundColor: "#272E48",
                    alignItems: 'center',
                    justifyContent: 'flex-start'
                }}>
                    <VStack style={{ marginTop: "10%", width: "70%", alignItems: 'center', backgroundColor: "#e03333", borderRadius: 15 }}>
                        <Text style={{ width: "70%", fontSize: 20, textAlign: 'center', marginVertical: "2%", color: "#fff" }}>Controle Interno</Text>
                    </VStack>
                    <Divider style={{ marginVertical: '2%', backgroundColor: "#354787" }} />
                    <Box safeAreaTop style={{
                        width: "90%", marginTop: "10%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 10,
                    }}>
                        <VStack style={{ alignItems: 'center', width: "90%" }}>
                            <Button variant='solid' style={{ width: "90%", borderRadius: 25, backgroundColor: '#37b31c' }} onPress={() => this.setState({ showJob: true })}><Text style={{ color: '#000' }}>Cadastrar Cargo</Text></Button>
                        </VStack>
                        <VStack style={{ alignItems: 'center', width: "90%", marginTop: '4%' }}>
                            <Button variant='solid' style={{ width: "90%", borderRadius: 25, backgroundColor: '#37b31c' }} onPress={() => this.setState({ showUser: true })}><Text style={{ color: '#000' }}>Cadastrar Colaborador</Text></Button>
                        </VStack>
                        <VStack style={{ alignItems: 'center', width: "90%", marginTop: '4%' }}>
                            <Button variant='solid' style={{ width: "90%", borderRadius: 25, backgroundColor: '#37b31c' }} onPress={() => this.setState({ showWar: true })}><Text style={{ color: '#000' }}>Cadastrar Avisos</Text></Button>
                        </VStack>
                        <VStack style={{ alignItems: 'center', width: "90%", marginTop: '4%' }}>
                            <Button variant='solid' style={{ width: "90%", borderRadius: 25, backgroundColor: '#37b31c' }} onPress={() => this.setState({ showVaga: true })}><Text style={{ color: '#000' }}>Cadastrar Vagas Abertas</Text></Button>
                        </VStack><VStack style={{ alignItems: 'center', width: "90%", marginTop: '4%', marginBottom: "10%" }}>
                            <Button variant='solid' style={{ width: "90%", borderRadius: 25, backgroundColor: '#d0e033' }} onPress={() => this.setState({ showAlt: true })}><Text style={{ color: '#000' }}>Alterar Cargo De Colaborador</Text></Button>
                        </VStack>
                    </Box>
                </View>
            </NativeBaseProvider>
        );
    }
}