import { Box, NativeBaseProvider, ScrollView, Divider, HStack, VStack } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Text, Image, } from 'react-native';
import * as React from 'react';

import api from '../../services/api'
import Global from '../Components/Global'

export default class Colabs extends React.Component {

    state = {
        data: new Array(),
        token: Global.token,
        idUser: Global.idUser,
        url: Global.url,
    }

    componentDidMount = async () => {
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        this.setState({
            token: valueToken
        })
        this.getUsers()
    }

    getUsers = async () => {
        const response = await api.get(`/user/${this.state.token}`)
        if (response.data.Users != null) {
            this.setState({
                data: response.data.Users
            })
        }
        setTimeout(() => {
            this.getUsers()
        }, 20000)
    }

    render() {
        const { data, url } = this.state

        return (
            <NativeBaseProvider>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "#272E48"
                }}>
                    <VStack style={{ marginTop: "10%", width: "70%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 45 }}>
                        <Text style={{ width: "70%", fontSize: 20, textAlign: 'center', marginVertical: "2%", color: "#fff" }}>Colaboradores </Text>
                    </VStack>
                    <Divider style={{ marginVertical: '2%', backgroundColor: "#354787" }} />

                    <ScrollView >
                        {data.map((value, index) => (
                            <View key={index}>
                                <Box style={{
                                    marginBottom: 5, borderRadius: 10, backgroundColor: "#354787", padding: 10
                                }}>
                                    <HStack style={{ marginBottom: "3%" }}>
                                        <VStack>
                                            <Image alt="Imagem do Usuario" source={{ uri: url + value.image }}
                                                style={{ width: 35, height: 35, borderRadius: 45, marginRight: 10 }} />
                                        </VStack>
                                        <VStack style={{ width: "87%" }}>
                                            <Text style={{ color: "#fff", fontSize: 17, marginTop: -5 }}>{value.name}</Text>
                                            <Text style={{ color: "#fff", marginTop: 5, fontSize: 15 }}>{value.position}</Text>
                                        </VStack>
                                    </HStack>
                                </Box>
                                <Divider my={2} />
                            </View>
                        ))}
                    </ScrollView>
                </View >
            </NativeBaseProvider >
        )
    }
}