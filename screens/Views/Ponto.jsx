import AsyncStorage from '@react-native-async-storage/async-storage';
import MapView, { Circle, Marker } from 'react-native-maps';
import * as TaskManager from "expo-task-manager"
import { View, Text } from 'react-native';
import * as Location from "expo-location";
import React, { useState, useEffect } from 'react';
import moment from 'moment';
import {
    Box,
    NativeBaseProvider,
    Button,
    VStack,
    Toast,
    HStack,
} from 'native-base';

import 'moment/locale/pt-br';

import api from '../../services/api'
import Global from './../Components/Global';

const user = []

async function getDates() {
    user.push({
        token: await AsyncStorage.getItem('@workflows:token'),
        idUser: await AsyncStorage.getItem('@workflows:idUser')
    })
    return
}
getDates()

const LOCATION_TASK_NAME = "LOCATION_TASK_WORKFLOWS"

TaskManager.defineTask(LOCATION_TASK_NAME, async ({ data, error }) => {
    if (error) {
        console.error(error)
        return
    }
    if (data) {
        const { locations } = data
        const location = locations[0]
        sendPositions(location)
    }
})
async function sendPositions(positions) {
    await api.post(`/positions/${user[0].token}`, {
        user_id: user[0].idUser,
        lat: positions.coords.latitude,
        long: positions.coords.longitude,
        speed: positions.coords.speed.toFixed(2),
        accuracy: positions.coords.accuracy.toFixed(2),
    })
}

export default function Ponto() {

    const [idUser, setIdUser] = useState(Global.idUser)
    const [token, setToken] = useState(Global.token)
    const [location, setLocation] = useState(Global.location)
    const [data, setData] = useState(new Array())
    const [date, setDate] = useState(new Date())
    const [pointEnd, setPointEnd] = useState(Boolean)
    const [position, setPosition] = useState({ latitude: -31.76996, longitude: -52.34093 })
    const [eLat, setELat] = useState(-31.755757320997137)
    const [eLon, setELon] = useState(-52.30392268378447)
    const [eDistance, setEDistance] = useState(Number(10))

    function tick() {
        setDate(new Date())
    }
    useEffect(() => {
        const requestPermissions = async () => {
            const foreground = await Location.requestForegroundPermissionsAsync()
            if (foreground.granted) await Location.requestBackgroundPermissionsAsync()
        }


        requestPermissions()
        getPoint()
        getLocation()
        getPosition()
        setInterval(
            () => tick(),
            1000 //1 segundo
        );

    }, [])
    async function getPosition() {
        setToken(await AsyncStorage.getItem('@workflows:token'))
        await Location.watchPositionAsync(
            {
                // Começa a observar a posição em tempo real
                accuracy: Location.Accuracy.BestForNavigation,
                timeInterval: 6000,
            },
            location => {
                setPosition({
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                })
            }
        )
    }
    async function getPoint() {
        const response = await api.post(`/userPoint/${token}`, {
            user_id: idUser
        })
        if (response.data.data[0] != null) {

            setData(response.data.data)
            setPointEnd(response.data.data[0].workpoint)
        } else {
            setPointEnd(1)
        }

        // setTimeout(() => {
        //     getPoint()
        //     getLocation()
        // }, 3000)
    }
    async function getLocation() {
        const response = await api.get(`/company/${token}`)
        if (response.data.data != null) {
            setEDistance(response.data.data[0].distance)
            setELat(response.data.data[0].lat)
            setELon(response.data.data[0].lon)
        } else {
            getLocation()
        }
    }
    async function sendPoint(workpoint) {
        const response = await api.post(`/point/${token}`, {
            idUser: idUser,
            workpoint: workpoint
        })
        if (response.data.erro === false) {
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            getPoint()
        } else {
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            getPoint()
        }
    }
    // Parar o rastreamento de localização em primeiro plano
    async function startBackgroundUpdate() {
        // Não rastreie a posição se a permissão não for concedida
        const { granted } = await Location.getBackgroundPermissionsAsync()
        if (!granted) {
            console.log("Servide de localização negado")
            return
        }

        // Verifique se a tarefa está definida, caso contrário, não inicie o rastreamento
        const isTaskDefined = await TaskManager.isTaskDefined(LOCATION_TASK_NAME)
        if (!isTaskDefined) {
            console.log("A tarefa não está definida")
            return
        }

        // Não rastreie se já estiver sendo executado em segundo plano
        const hasStarted = await Location.hasStartedLocationUpdatesAsync(LOCATION_TASK_NAME)

        if (hasStarted) {
            console.log("Serviço Já Iniciado")
            return
        }

        await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
            // Para melhores registros, definimos a precisão para a opção mais sensível
            accuracy: Location.Accuracy.BestForNavigation,
            // Certifique-se de ativar esta notificação se quiser acompanhar consistentemente em segundo plano
            showsBackgroundLocationIndicator: true,
            timeInterval: 15000,
            foregroundService: {
                notificationTitle: "WorkFlows",
                notificationBody: "Localização sendo compartilhada",
            },
        })
    }
    // Parar o rastreamento de localização em segundo plano
    async function stopBackgroundUpdate() {
        const hasStarted = await Location.hasStartedLocationUpdatesAsync(
            LOCATION_TASK_NAME
        )
        if (hasStarted) {
            await Location.stopLocationUpdatesAsync(LOCATION_TASK_NAME)
            console.log("Serviço de Localização Finalizado!")
        }
    }
    async function initPoint(workpoint) {
        if (workpoint === 0) {
            if (location === 0) {
                let result = getDistanceFromLatLonInKm()
                if (result > eDistance) {
                    Toast.show({
                        title: "Você esta distante da empresa!",
                        description: "Aproximesse para iniciar o expediente!",
                        placement: "top",
                        style: {
                            backgroundColor: "#354787",
                            fontSize: 20
                        }
                    })
                } else {
                    sendPoint(workpoint)
                }
            } else {
                sendPoint(workpoint)
                startBackgroundUpdate()
            }
        }
        if (workpoint === 1) {
            sendPoint(workpoint)
            stopBackgroundUpdate()
        }
    }
    function getDistanceFromLatLonInKm() {
        "use strict";
        var deg2rad = function (deg) { return deg * (Math.PI / 180); },
            R = 6371,
            dLat = deg2rad(position.latitude - eLat),
            dLng = deg2rad(position.longitude - eLon),
            a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(deg2rad(eLat))
                * Math.cos(deg2rad(eLon))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return ((R * c * 1000).toFixed());
    }

    return (
        <NativeBaseProvider>
            <View style={{
                flex: 1,
                resizeMode: "cover",
                backgroundColor: "#272E48",
                alignItems: 'center',
                justifyContent: 'flex-start',
            }}>
                <Box style={{
                    marginTop: "10%",
                    justifyContent: 'flex-start',
                    width: "90%",
                    height: "25%",
                    alignItems: 'center',
                    backgroundColor: "#354787",
                    borderTopStartRadius: 10,
                    borderTopEndRadius: 10,
                }}>
                    <VStack
                        style={{
                            marginTop: "3%",
                            width: "70%",
                            height: "20%",
                            alignItems: 'center',
                            backgroundColor: "#272E48",
                            borderRadius: 15,
                            marginBottom: "5%"
                        }}>
                        <Text
                            style={{
                                width: "90%",
                                height: "90%",
                                fontSize: 17,
                                justifyContent: 'center',
                                textAlign: 'center',
                                marginTop: "3%",
                                color: "#fff"
                            }}>Ponto do Usuario</Text>
                    </VStack>
                    <VStack
                        style={{
                            justifyContent: "center",
                            alignItems: 'center',
                            width: "99%",
                            height: "25%",
                            marginTop: "2%",
                            backgroundColor: '#272E48'
                        }}>
                        <Text
                            style={{
                                fontSize: 30,
                                color: "#bf740e"
                            }}>{date.toLocaleTimeString()}</Text>
                    </VStack>

                    <VStack
                        style={{
                            alignItems: 'center',
                            width: "60%",
                            height: "25%",
                            marginTop: "6%"
                        }}>
                        {pointEnd === 1 ?
                            <Button
                                variant='solid'
                                style={{
                                    width: "90%",
                                    height: "90%",
                                    borderRadius: 25,
                                    backgroundColor: "#bf740e"
                                }}
                                onPress={() => initPoint(0)}><Text style={{ color: '#fff' }}>Inicio de Expediente</Text></Button>
                            :
                            <Button
                                variant='solid'
                                style={{
                                    width: "90%",
                                    height: "90%",
                                    borderRadius: 25,
                                    backgroundColor: "#C0C0C0"
                                }}
                                onPress={() => initPoint(1)}><Text style={{ color: "#000" }}>Fim de Expediente</Text></Button>
                        }

                    </VStack>
                </Box>
                <Box style={{
                    width: "90%",
                    height: "50%",
                    marginTop: "2%",
                    alignItems: 'center',
                    backgroundColor: "#354787",
                    borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10,
                }}>
                    <Box style={{ width: "100%", height: "100%", alignItems: 'center' }}>
                        <MapView style={{ width: "100%", height: "100%", justifyContent: 'center', alignItems: 'center' }}
                            // mapType={mapType}
                            // zoomControlEnabled={true}
                            loadingEnabled={true}
                            showsUserLocation={true}
                            userLocationPriority={'high'}
                            userLocationUpdateInterval={30000}
                            // Region seta a região inicial do mapa, sem isso o mapa da erro.
                            region={{
                                latitude: position.latitude,
                                longitude: position.longitude,
                                latitudeDelta: 0.002,
                                longitudeDelta: 0.002

                            }}
                        >
                            <Circle
                                center={{
                                    latitude: eLat,
                                    longitude: eLon
                                }}
                                radius={eDistance}
                                fillColor={'rgba(02,63,142,0.3)'}
                                strokeWidth={0}
                                style={{ opacity: 1 }}
                            />
                            <Marker
                                coordinate={{ latitude: eLat, longitude: eLon }}
                                pinColor="#354787"
                                title="Work Flows"
                            />
                        </MapView>
                    </Box>
                </Box>
                <Box style={{
                    width: "90%",
                    marginTop: "2%",
                    alignItems: 'center',
                    backgroundColor: "#354787",
                    borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10,
                }}>

                    <VStack
                        style={{
                            marginTop: "3%",
                            width: "70%",
                            alignItems: 'center',
                            backgroundColor: "#272E48",
                            borderRadius: 15,
                            marginBottom: "4%"
                        }}>
                        <Text
                            style={{
                                width: "90%",
                                // height: "90%",
                                fontSize: 17,
                                textAlign: 'center',
                                marginTop: 2,
                                color: "#fff"
                            }}>Ultimos Pontos</Text>
                    </VStack>

                    {data[0] != null ?
                        data[1] != null &&
                        <VStack
                            style={{
                                alignItems: "center",
                                justifyContent: "center",
                                width: "70%",
                                marginBottom: "3%"
                                // height: "5%"
                            }}>
                            {data[0].workpoint === 0 ?
                                <HStack style={{ backgroundColor: "#bf740e", alignItems: "center", marginHorizontal: '2%', width: '99%', justifyContent: "center", marginVertical: "2%", borderRadius: 45 }}>
                                    <Text style={{ color: "#fff", marginVertical: "2%" }}>Entrada</Text>
                                    <Text style={{ marginLeft: "2%", color: "#fff" }}>
                                        {`${moment(data[1].created_at).format("LTS")}  ${moment(data[1].created_at).format("L")}`}
                                    </Text>
                                </HStack>
                                :
                                <HStack style={{ backgroundColor: "#C0C0C0", alignItems: "center", marginHorizontal: '2%', width: '99%', justifyContent: "center", marginVertical: "2%", borderRadius: 45 }}>
                                    <Text
                                        style={{ marginRight: 20, color: "#000", marginVertical: "2%" }}>Saida</Text>
                                    <Text style={{ marginLeft: "2%", color: "#000" }}>
                                        {`${moment(data[1].created_at).format("LTS")}  ${moment(data[1].created_at).format("L")}`}
                                    </Text>

                                </HStack>
                            }
                            {data[1].workpoint === 0 ?
                                <HStack style={{ backgroundColor: "#bf740e", alignItems: "center", marginHorizontal: '2%', width: '99%', justifyContent: "center", marginVertical: "2%", borderRadius: 45 }}>
                                    <Text style={{ color: "#fff", marginVertical: "2%" }}>Entrada</Text>
                                    <Text style={{ marginLeft: "2%", color: "#fff" }}>
                                        {`${moment(data[1].created_at).format("LTS")}  ${moment(data[1].created_at).format("L")}`}
                                    </Text>
                                </HStack>
                                :
                                <HStack style={{ backgroundColor: "#C0C0C0", alignItems: "center", marginHorizontal: '2%', width: '99%', justifyContent: "center", marginVertical: "2%", borderRadius: 45 }}>
                                    <Text style={{ marginRight: 20, color: "#000", marginVertical: "2%" }}>Saida</Text>
                                    <Text style={{ marginLeft: "2%", color: "#000" }}>
                                        {`${moment(data[1].created_at).format("LTS")}  ${moment(data[1].created_at).format("L")}`}
                                    </Text>

                                </HStack>
                            }
                        </VStack>
                        : <VStack
                            style={{
                                alignItems: 'center',
                                width: "80%",
                                // height: "40%"
                            }}>
                            <Text style={{ fontSize: 22, color: "#fff" }}>Sem Pontos</Text>
                        </VStack>
                    }
                </Box>
            </View>
        </NativeBaseProvider>
    );
    // }
}