import { Box, NativeBaseProvider, ScrollView, Divider, HStack, } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { View, Text } from 'react-native';
import * as React from 'react';
import api from '../../services/api'

export default class Warning extends React.Component {

    state = {
        data: new Array(),
        showModal: Boolean,
        isOpen: Boolean,
        isStatusOpen: Boolean,
        statusMessage: String,
        statusImage: Boolean,
        title: String,
        description: String,
        weight: Number,
        token: Number,
        idwarning: Number
    }

    componentDidMount = async () => {
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        this.setState({
            token: valueToken
        })
        this.getWarnings()
    }

    getWarnings = async () => {
        const response = await api.get(`/warning/${this.state.token}`)
        this.setState({
            data: response.data.warnings
        })

        setTimeout(() => {
            this.getWarnings()
        }, 20000)
    }

    render() {
        const { data } = this.state

        return (
            <NativeBaseProvider>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#272E48"
                }}>
                    <HStack style={{ marginTop: "15%", justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{
                            paddingLeft: 25, paddingRight: 25, paddingBottom: 10, paddingTop: 10,
                            borderRadius: 5, backgroundColor: "#354787", color: "#fff", fontSize: 19, borderRadius:45
                        }}>Quadro de Avisos</Text>
                    </HStack>
                    <ScrollView style={{ marginTop: "5%", paddingLeft: 10, paddingRight: 10, width: "95%" }}>
                        {data[0] != null ? data.map((value, index) => (
                            value.weight === 1 ?

                                // Avisos em Vermelho
                                <View key={index}>
                                    <Box key={index} style={{
                                        marginBottom: 5, borderRadius: 10, borderColor: "#FF0000",
                                        backgroundColor: "#354787", borderWidth: 2, padding: 10
                                    }}>
                                        <View style={{ flexDirection: "row", alignSelf: "center" }}>
                                            <Text style={{ color: "#fff", fontSize: 20 }}>
                                                <Ionicons name='warning-outline' size={25} color={"#FF0000"} />
                                            </Text>
                                            <Text style={{ color: "#fff", fontSize: 20, marginLeft: 5 }}>{value.title}</Text>
                                            {/* <Button marginRight={3} style={{ backgroundColor: "#FF0000", marginLeft: 15 }} onPress={() => delW(value.idwarning)}><Ionicons name='trash-outline' size={15} color={"#fff"} /></Button> */}
                                        </View>
                                        <Text style={{ color: "#fff", padding: 5, marginTop: 3 }}>{value.description}</Text>
                                    </Box>
                                    <Divider my={2} />
                                </View>
                                : value.weight === 2 ?

                                    // Avisos Em Amarelo
                                    <View key={index}>
                                        <Box style={{
                                            marginBottom: 5, borderRadius: 10, backgroundColor: "#354787", borderColor: "#FFFF00",
                                            borderWidth: 2, padding: 10
                                        }}>
                                            <View style={{ flexDirection: "row", alignSelf: "center" }}>
                                                <Text style={{ color: "#fff", fontSize: 20, marginRight: 5, textAlign: "left" }}>
                                                    <Ionicons name='alert-circle-outline' size={25} color={"#FFFF00"} /></Text>
                                                <Text style={{ color: "#fff", fontSize: 20, textAlign: "center" }}>{value.title}</Text>
                                                {/* <Button marginRight={3} style={{ backgroundColor: "#FF0000", marginLeft: 15 }} onPress={() => delW(value.idwarning)}><Ionicons name='trash-outline' size={15} color={"#fff"} /></Button> */}
                                            </View>
                                            <Text style={{ color: "#fff", padding: 5, marginTop: 3 }}>{value.description}</Text>
                                        </Box>
                                        <Divider my={2} />
                                    </View>
                                    :

                                    //Avisos em Azul
                                    <View key={index}>
                                        <Box key={index} style={{
                                            marginBottom: 5, borderRadius: 10, backgroundColor: "#354787",
                                            borderColor: "#00BFFF", borderWidth: 2, padding: 10
                                        }}>
                                            <View style={{ flexDirection: "row", alignSelf: "center" }}>
                                                <Text style={{ color: "#fff", fontSize: 20, marginRight: 5, textAlign: "left" }}>
                                                    <Ionicons name='information-circle-outline' size={25} color={"#00BFFF"} /></Text>
                                                <Text style={{ color: "#fff", fontSize: 20 }}>{value.title}</Text>
                                                {/* <Button marginRight={3} style={{ backgroundColor: "#FF0000", marginLeft: 15 }} onPress={() => delW(value.idwarning)}><Ionicons name='trash-outline' size={15} color={"#fff"} /></Button> */}
                                            </View>
                                            <Text style={{ color: "#fff", padding: 5, marginTop: 3 }}>{value.description}</Text>
                                        </Box>
                                        <Divider my={2} />
                                    </View>
                        )) : <Text style={{ width: "99%", textAlign: 'center', color: "#fff", fontSize: 28 }}>Sem Avisos! :]</Text>}
                    </ScrollView>
                </View >
            </NativeBaseProvider >
        )
    }
}