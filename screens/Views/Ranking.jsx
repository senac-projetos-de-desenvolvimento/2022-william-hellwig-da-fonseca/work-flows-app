import { Box, NativeBaseProvider, ScrollView, Divider, HStack, VStack, } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Text, Image, } from 'react-native';
import * as React from 'react';

import api from '../../services/api'
import Global from '../Components/Global'

export default class Ranking extends React.Component {

    state = {
        data: new Array(),
        idUser: Global.idUser,
        token: Global.token,
        url: Global.url,
    }

    componentDidMount = async () => {
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        this.setState({
            token: valueToken
        })
        this.getUsers()
    }
    getUsers = async () => {
        const response = await api.get(`/score/${this.state.token}`)
        if (response.data.Users != null) {
            this.setState({
                data: response.data.Users
            })
        }
        setTimeout(() => {
            this.getUsers()
        }, 20000)
    }


    render() {
        const { data, url } = this.state

        return (
            <NativeBaseProvider>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "#272E48"
                }}>
                    <VStack style={{ marginTop: "10%", width: "70%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 45 }}>
                        <Text style={{ width: "70%", fontSize: 20, textAlign: 'center', marginVertical: "2%", color: "#fff" }}>Ranking</Text>
                    </VStack>
                    <Divider style={{ marginVertical: '2%', backgroundColor: "#354787" }} />

                    <ScrollView >
                        {data.map((value, index) => (
                            <View key={index}>
                                <Box style={{
                                    marginVertical: "2%",
                                    width: "98%",
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Box style={{ width: "99%", borderRadius: 10, backgroundColor: "#354787", padding: 10 }}>
                                        <HStack space={3} style={{ width: "80%" }}>
                                            <VStack style={{ justifyContent: "center" }}>
                                                {index === 0 ?
                                                    <Text style={{ color: "#FFD700", fontSize: 20 }}>{index + 1}º</Text>
                                                    : index === 1 ?
                                                        <Text style={{ color: "#C0C0C0", fontSize: 20 }}>{index + 1}º</Text>
                                                        : index === 2 ?
                                                            <Text style={{ color: "#cd7f32", fontSize: 20 }}>{index + 1}º</Text>
                                                            :
                                                            <Text style={{ color: "#fff", fontSize: 20 }}>{index + 1}º</Text>

                                                }
                                            </VStack>
                                            <VStack style={{ justifyContent: "center" }}>
                                                <Image alt="Imagem do Usuario" source={{ uri: url + value.image }}
                                                    style={{ width: 50, height: 50, borderRadius: 45, marginRight: 10 }} />
                                            </VStack>
                                            <VStack style={{ width: "87%", justifyContent: 'center' }}>
                                                <Text style={{ color: "#fff", fontSize: 17, marginTop: -5 }}>{value.name}</Text>
                                                {index === 0 ?
                                                    <Text style={{ color: "#FFD700", marginTop: 5, fontSize: 15 }}>{value.score} Pontos</Text>
                                                    : index === 1 ?
                                                        <Text style={{ color: "#C0C0C0", marginTop: 5, fontSize: 15 }}>{value.score} Pontos</Text>
                                                        : index === 2 ?
                                                            <Text style={{ color: "#cd7f32", marginTop: 5, fontSize: 15 }}>{value.score} Pontos</Text>
                                                            :
                                                            <Text style={{ color: "#fff", marginTop: 5, fontSize: 15 }}>{value.score} Pontos</Text>
                                                }
                                            </VStack>
                                        </HStack>
                                    </Box>
                                </Box>
                                <Divider style={{ backgroundColor: "#354787", marginVertical: "2%" }} />
                            </View>
                        ))}
                    </ScrollView>
                </View >
            </NativeBaseProvider >
        )
    }
}