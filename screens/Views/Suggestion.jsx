import { Box, NativeBaseProvider, ScrollView, Divider, Button, HStack, Modal, FormControl, TextArea, VStack, WarningOutlineIcon, Toast } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Text, Image, TouchableOpacity, } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as React from 'react';
import moment from 'moment';

import api from '../../services/api'
import Global from '../Components/Global'

export default class Suggestions extends React.Component {

    state = {
        data: new Array(),
        idUser: Global.idUser,
        token: Global.token,
        url: Global.url,
        suggestion: '',
        erro: false,
        showDenuncia: Boolean,
        reportDescription: "",
        erroMsg: '',
        showModal: Boolean,
        type: null
    }

    componentDidMount = async () => {
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        this.setState({
            token: valueToken
        })
        this.getSuggestion()
    }
    getSuggestion = async () => {
        const response = await api.get(`/suggestion/${this.state.token}`)
        this.setState({
            data: response.data.data
        })

        setTimeout(() => {
            this.getSuggestion()
        }, 20000)
    }
    sendSuggestion = async () => {
        const response = await api.post(`/suggestion/${this.state.token}`, {
            id: this.state.idUser,
            suggestion: this.state.suggestion

        })
        if (response.data.erro === false) {
            this.setState({
                showModal: false,
                suggestion: ''
            })
            this.getSuggestion();
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        } else {
            this.setState({
                showModal: false,
                suggestion: ''
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }
    sendReport = async () => {
        const response = await api.post(`/report/${this.state.token}`, {
            id: this.state.idReport,
            type: this.state.type,
            description: this.state.reportDescription
        })
        if (response.data.erro === false) {
            this.setState({
                reportDescription: String,
                showDenuncia: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            this.getFeed()
        } else {
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }
    handleSuggestionChange = (suggestion) => {
        this.setState({ suggestion: suggestion })
    }
    handleReportChange = (reportDescription) => {
        this.setState({ reportDescription: reportDescription })
    }

    render() {
        const { data, url, erro, erroMsg, suggestion } = this.state

        const cadSug = () => {
            if (suggestion == '') {
                this.setState({
                    erroMsg: "Você deve informar sua ideia!",
                    erro: true,
                })
            } else {
                this.sendSuggestion();
            }
        }
        const openReport = (idReport, type) => {
            this.setState({
                showDenuncia: true,
                idReport: idReport,
                type: type
            })
        }
        return (
            <NativeBaseProvider>
                <Modal
                    isOpen={this.state.showModal}
                    onClose={() => this.setState({ showModal: false })}
                    size={'full'}
                    style={{ height: "60%" }}
                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Body>
                            <FormControl isInvalid={erro} mt="3">
                                <TextArea require placeholder='Qual Sua Sugestão?' fontSize={16} color={'black'} variant="underlined"
                                    value={this.state.suggestion} onChangeText={this.handleSuggestionChange} />
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{
                                alignSelf: "center", width: "40%", height: "25%",
                                margin: "5%", textAlign: "center", fontSize: 16, backgroundColor: '#354787', borderRadius:45
                            }} onPress={() => cadSug()}>Adicionar</Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Denucia */}
                <Modal
                    isOpen={this.state.showDenuncia}
                    onClose={() => this.setState({ showDenuncia: false })}
                    size={'full'}
                    style={{ height: "60%" }}
                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Body>
                            <FormControl isInvalid={erro} mt="3">
                                <TextArea require
                                    placeholder='Digite o Motivo Da Denuncia...'
                                    fontSize={16}
                                    color={'black'}
                                    variant="underlined"
                                    value={this.state.reportDescription}
                                    onChangeText={this.handleReportChange} />
                            </FormControl>
                            <Button style={{
                                alignSelf: "center",
                                width: "40%",
                                height: "25%",
                                margin: "5%",
                                textAlign: "center",
                                fontSize: 16,
                                backgroundColor: "#354787",
                                borderRadius:45
                            }}
                                onPress={() => this.sendReport()}>Denunciar</Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "#272E48"
                }}>
                    <VStack style={{ marginTop: "10%", width: "70%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 45 }}>
                        <Text style={{ width: "70%", fontSize: 20, textAlign: 'center', marginVertical: "2%", color: "#fff" }}>Quadro De Ideias</Text>
                    </VStack>
                    <VStack style={{ marginTop: "6%", width: "90%" }}>
                        <Button style={{ borderRadius: 45, backgroundColor: '#354787' }}
                            onPress={() => this.setState({ showModal: true })}>Adicionar nova ideia?</Button>
                    </VStack>
                    <Divider style={{ marginVertical: '2%', backgroundColor: "#354787" }} />

                    <ScrollView style={{ width: "95%" }}>
                        {data[0] != null ? data.map((value, index) => (
                            <View key={index}>
                                <Box style={{
                                    marginBottom: 5, borderRadius: 10, backgroundColor: "#354787", padding: 10
                                }}>
                                    <HStack style={{ marginBottom: "3%" }}>
                                        <VStack>
                                            <Image alt="Imagem do Usuario" source={{ uri: url + value.image }}
                                                style={{ width: 35, height: 35, borderRadius: 45, marginRight: 10 }} />
                                        </VStack>
                                        <VStack style={{ width: "82%" }}>
                                            <Text style={{ color: "#fff", fontSize: 17, marginTop: -5 }}>{value.name}</Text>
                                            <Text style={{
                                                color: "#B5B5B2", fontSize: 10, marginTop: -3,
                                                fontStyle: "italic", textDecorationLine: "underline"
                                            }}>{moment(value.created_at).format("DD/MM/YY HH:MM")}</Text>
                                            <Text style={{ color: "#fff", marginTop: 5, fontSize: 15 }}>{value.suggestion}</Text>
                                        </VStack>
                                        <VStack style={{ marginTop: "2%" }}>
                                            <TouchableOpacity variant={'ghost'} onPress={() => openReport(value.idsuggestion, 1)}>
                                                <Ionicons name='md-warning-outline' size={16} color={"#fff"} />
                                            </TouchableOpacity>
                                        </VStack>
                                    </HStack>
                                </Box>
                                <Divider my={2} />
                            </View>
                        )) : <Text style={{ width: "99%", textAlign: 'center', color: "#fff", fontSize: 28 }}>Estamos Sem ideias! :(</Text>}
                    </ScrollView>
                </View >
            </NativeBaseProvider >
        )
    }
}