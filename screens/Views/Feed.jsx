import * as React from 'react';
import { View, Text, Pressable, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    Box,
    NativeBaseProvider,
    ScrollView,
    Divider,
    Button,
    HStack,
    TextArea,
    VStack,
    Toast,
    Image,
    Modal,
    FormControl,
    WarningOutlineIcon
} from 'native-base';

import Ionicons from 'react-native-vector-icons/Ionicons';

import api from '../../services/api'
import Global from './../Components/Global';
import moment from 'moment';
import Helpers from '../Components/Helpers';

export default class Feed extends React.Component {

    state = {
        data: new Array(),
        idUser: Global.idUser,
        token: "",
        pushToken: '',
        isStatusOpen: Boolean,
        publication: "",
        idPublication: Number,
        idReport: Number,
        comment: "",
        show: Boolean,
        showModal: Boolean,
        showDenuncia: Boolean,
        reportDescription: "",
        erro: false,
        erroMsg: '',
        user: '',
        pass: '',
        baseURL: api.defaults.baseURL,
        type: null
    }

    componentDidMount = async () => {
        Helpers.registerPushNotifications()
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        let pushToken = await AsyncStorage.getItem('@workflows:pushToken')
        let user = await AsyncStorage.getItem("@workflows:login")
        let pass = await AsyncStorage.getItem("@workflows:password")
        let idU = await AsyncStorage.getItem("@workflows:idUser")
        this.setState({
            token: valueToken,
            pushToken: pushToken,
            user: user,
            pass: pass,
            idUser: idU
        })
        this.getFeed()
        this.sendPushToken()
    }
    sendPushToken = async () => {
        const response = await api.post(`/sendpush/${this.state.token}`, {
            pushToken: this.state.pushToken,
            idUser: this.state.idUser
        })
        if (response.data.erro === true) {
            setTimeout(() => {
                this.sendPushToken()
            }, 15000);
        }
    }
    verifyToken = async (error) => {
        if (error.aut === true) {

            const response = await api.post('/login', {
                email: this.state.user,
                password: this.state.pass,
                pushToken: this.state.pushToken
            })
            if (response.data.erro === false) {

                Global.name = response.data.data.name
                Global.image = response.data.data.image
                Global.position = response.data.data.position
                Global.location = response.data.data.location
                Global.score = response.data.data.score
                Global.token = response.data.data.token
                Global.idUser = response.data.data.user_id
                Global.pushToken = pushToken

                await AsyncStorage.setItem('@workflows:token', String(response.data.data.token))
                await AsyncStorage.setItem('@workflows:idUser', String(response.data.data.user_id))
                await AsyncStorage.setItem('@workflows:login', String(email))
                await AsyncStorage.setItem('@workflows:password', String(password))

                this.getFeed()

            } else {
                Toast.show({
                    title: "Faça login para continuar!",
                    placement: "top",
                    style: {
                        backgroundColor: "#354787",
                        fontSize: 20
                    }
                })
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }]
                })
            }
        }
    }
    getFeed = async () => {
        const response = await api.get(`/feed/${this.state.token}`)
        if (response.data.aut === true) {
            this.verifyToken(response.data)
        }
        if (response.data.data != null) {
            this.setState({
                data: response.data.data
            })
        }

        setTimeout(() => {
            this.getFeed()
        }, 60000)
    }
    sendPubli = async () => {
        const response = await api.post(`/feed/${this.state.token}`, {
            id: this.state.idUser,
            publication: this.state.publication,
            score: Global.score - 15
        })
        if (response.data.erro === false) {
            this.setState({
                publication: "",
                showModal: false,
            })
            Global.score = Global.score - 15
            Toast.show({
                title: response.data.data,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            this.getFeed()
        } else {
            this.setState({
                publication: "",
                showModal: false,
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }
    sendCommet = async () => {
        const response = await api.post(`/comment/${this.state.token}`, {
            idUser: this.state.idUser,
            idPublication: this.state.idPublication,
            comment: this.state.comment,
            score: Global.score + 20
        })
        if (response.data.erro === false) {
            this.setState({
                comment: String,
                show: false
            })
            Global.score = Global.score + 20
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            this.getFeed()
        }
    }
    sendReport = async () => {
        const response = await api.post(`/report/${this.state.token}`, {
            id: this.state.idReport,
            type: this.state.type,
            description: this.state.reportDescription
        })
        if (response.data.erro === false) {
            this.setState({
                reportDescription: String,
                showDenuncia: false
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
            this.getFeed()
        } else {
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }

    handlePublicationChange = (publication) => {
        this.setState({ publication: publication })
    }
    handleCommentChange = (comment) => {
        this.setState({ comment: comment })
    }
    handleReportChange = (reportDescription) => {
        this.setState({ reportDescription: reportDescription })
    }

    render() {
        const { data, baseURL, publication, erro, erroMsg } = this.state

        const openPubli = () => {
            if (publication == '') {
                this.setState({
                    erroMsg: "Você deve informar algo para continuar!",
                    erro: true,
                })
            } else {
                this.sendPubli();
            }
        }
        const openComment = (idPublication) => {
            this.setState({
                show: true,
                idPublication: idPublication
            })
        }
        const openReport = (idReport, type) => {
            this.setState({
                showDenuncia: true,
                idReport: idReport,
                type: type
            })
        }

        return (
            <NativeBaseProvider>
                {/* Modal Publicação */}
                <Modal
                    isOpen={this.state.showModal}
                    onClose={() => this.setState({ showModal: false })}
                    size={'full'}
                    style={{ justifyContent: 'center' }}
                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Body>
                            <FormControl isInvalid={erro} mt="3">
                                <TextArea require placeholder='Digite sua publicação...' fontSize={16} color={'black'} variant="underlined"
                                    value={this.state.publication} onChangeText={this.handlePublicationChange} />
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{
                                alignSelf: "center", width: "40%", height: "25%",
                                margin: "5%", textAlign: "center", fontSize: 16, backgroundColor: "#354787", borderRadius: 45
                            }} onPress={() => openPubli()}>Adicionar</Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Comentarios */}
                <Modal
                    isOpen={this.state.show}
                    onClose={() => this.setState({ show: false })}
                    size={'full'}
                    style={{ height: "60%" }}
                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Body>
                            <FormControl isInvalid={erro} mt="3">
                                <TextArea require placeholder='Digite Seu Comentario...' fontSize={16} color={'black'} variant="underlined" value={this.state.comment} onChangeText={this.handleCommentChange} />
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", height: "25%", margin: "5%", textAlign: "center", fontSize: 16, backgroundColor: "#354787", borderRadius: 45 }} onPress={() => this.sendCommet()}>Comentar</Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                {/* Modal Denucia */}
                <Modal
                    isOpen={this.state.showDenuncia}
                    onClose={() => this.setState({ showDenuncia: false })}
                    size={'full'}
                    style={{ height: "60%" }}
                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Body>
                            <FormControl isInvalid={erro} mt="3">
                                <TextArea require
                                    placeholder='Digite o Motivo Da Denuncia...'
                                    fontSize={16}
                                    color={'black'}
                                    variant="underlined"
                                    value={this.state.reportDescription}
                                    onChangeText={this.handleReportChange} />
                            </FormControl>
                            <Button style={{
                                alignSelf: "center",
                                width: "40%",
                                height: "25%",
                                margin: "5%",
                                textAlign: "center",
                                fontSize: 16,
                                backgroundColor: "#354787",
                                borderRadius: 45
                            }}
                                onPress={() => this.sendReport()}>Denunciar</Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "#272E48"
                }}>
                    <VStack style={{ marginTop: "15%", width: "90%" }}>
                        <Button style={{ borderRadius: 15, backgroundColor: "#354787", borderRadius: 45 }} onPress={() => this.setState({ showModal: true })}>Adicionar Publicação?</Button>
                    </VStack>
                    <Divider style={{ marginVertical: '2%', backgroundColor: "#354787" }} />

                    <ScrollView style={{ width: "95%" }}>
                        {data[0] != null ? data.map((value, index) => (
                            <View key={index}>
                                <Box style={{
                                    marginBottom: 5, borderRadius: 10, backgroundColor: "#354787", padding: 10
                                }}>
                                    <HStack style={{ marginBottom: "3%" }}>
                                        <VStack>
                                            <Image alt="Imagem do Usuario" source={{ uri: baseURL + value.image }}
                                                style={{ width: 35, height: 35, borderRadius: 45, marginRight: 10 }} />
                                        </VStack>
                                        <VStack style={{ width: "80%" }}>
                                            <Text style={{ color: "#fff", fontSize: 17, marginTop: -5 }}>{value.name}</Text>
                                            <Text style={{ color: "#B5B5B2", fontSize: 13, marginTop: -3, fontStyle: "italic", textDecorationLine: "underline" }}>{value.position}</Text>
                                            <Text style={{ color: "#B5B5B2", fontSize: 10, fontStyle: "italic" }}>{moment(value.created_at).format("DD/MM/YY HH:MM")}</Text>
                                            <Text style={{ color: "#fff", marginTop: 5, fontSize: 15 }}>{value.publication}</Text>
                                            <Button marginLeft={-5} h="10" w="16" variant={'ghost'} onPress={() => openComment(value.publication_id)}>
                                                <Ionicons name='chatbox-outline' size={20} color={"#fff"} /></Button>
                                        </VStack>
                                        <VStack style={{ marginTop: "2%" }}>
                                            <TouchableOpacity variant={'ghost'} onPress={() => openReport(value.publication_id, 3)}>
                                                <Ionicons name='md-warning-outline' size={16} color={"#fff"} />
                                            </TouchableOpacity>
                                        </VStack>
                                    </HStack>

                                    {value.comment && value.comment.map((val, ind) => (
                                        <HStack style={{ marginLeft: "5%", marginBottom: "3%" }} key={ind}>
                                            <VStack>
                                                <Image alt="Imagem do Usuario" source={{ uri: baseURL + val.image }}
                                                    style={{ width: 35, height: 35, borderRadius: 45, marginRight: 10 }} />
                                            </VStack>
                                            <VStack style={{ width: "80%" }}>
                                                <Text style={{ color: "#fff", fontSize: 17, marginTop: -5 }}>{val.name}</Text>
                                                <Text style={{ color: "#B5B5B2", fontSize: 13, marginTop: -3, fontStyle: "italic", textDecorationLine: "underline" }}>{val.position}</Text>
                                                <Text style={{ color: "#B5B5B2", fontSize: 10, fontStyle: "italic" }}>{moment(val.created_at).format("DD/MM/YY HH:MM")}</Text>
                                                <Text style={{ color: "#fff", marginTop: 5, fontSize: 15 }}>{val.comment}</Text>
                                            </VStack>
                                            <VStack style={{ marginTop: "2%" }}>
                                                <TouchableOpacity variant={'ghost'} onPress={() => openReport(val.idComment, 2)}>
                                                    <Ionicons name='md-warning-outline' size={16} color={"#fff"} />
                                                </TouchableOpacity>
                                            </VStack>
                                        </HStack>
                                    ))}
                                </Box>
                                <Divider my={2} />
                            </View>
                        )) : <Text style={{ width: "99%", textAlign: 'center', color: "#fff", fontSize: 28 }}>Sem Publicações!</Text>}
                    </ScrollView>
                </View >
            </NativeBaseProvider >
        )
    }
}