import { Box, NativeBaseProvider, ScrollView, Divider, HStack, VStack } from 'native-base';
import { View, Text, TouchableOpacity, Share, } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FontAwesome5 } from 'react-native-vector-icons';
import * as React from 'react';
import moment from 'moment';

import api from '../../services/api'
import Global from '../Components/Global'

export default class Vagas extends React.Component {

    state = {
        data: new Array(),
        idUser: Global.idUser,
        token: Global.token,
        url: Global.url,
        suggestion: '',
        erro: false,
        name: "Work Flows",
        email: "rh@workflows.com",
        erroMsg: '',
        showModal: Boolean,
    }

    componentDidMount = async () => {
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        this.setState({
            token: valueToken
        })
        this.getVagas()
    }
    onShare = async (position, message) => {
        try {
            const result = await Share.share({
                message:
                    `Empresa ${this.state.name}, Seleciona para vaga de ${position}. 
                    \n${message} 
                    \nInteressados enviar curriculo para: ${this.state.email}`,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                } else {
                }
            } else if (result.action === Share.dismissedAction) {
            }
        } catch (error) {
            alert(error.message);
        }
    }
    getVagas = async () => {
        const response = await api.get(`/vacancie/${this.state.token}`)
        if (response.data.data != null) {
            this.setState({
                data: response.data.data
            })
        }
        setTimeout(() => {
            this.getVagas()
        }, 5000)
    }

    render() {
        const { data } = this.state
        return (
            <NativeBaseProvider>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "#272E48"
                }}>
                    <VStack style={{ marginTop: "10%", width: "70%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 45 }}>
                        <Text style={{ width: "70%", fontSize: 20, textAlign: 'center', marginVertical: "2%", color: "#fff" }}>Quadro De Vagas</Text>
                    </VStack>
                    <Divider style={{ marginVertical: '2%', backgroundColor: "#354787" }} />

                    <ScrollView style={{ width: "90%" }}>
                        {data[0] != null ? data.map((value, index) => (
                            <View key={index}>
                                <Box style={{
                                    marginBottom: 5, borderRadius: 10, backgroundColor: "#354787", padding: 10, alignItems: 'center',
                                    width: "99%"
                                }}>
                                    <VStack style={{ width: "99%", alignItems: 'center' }}>
                                        <HStack style={{ width: "99%", alignItems: 'center', justifyContent: 'space-between' }} >
                                            <Text style={{ color: "#fff", fontSize: 22, }}>{value.position}</Text>
                                            <TouchableOpacity style={{
                                                alignSelf: "center",
                                                textAlign: "center",
                                            }}
                                                onPress={() => this.onShare(value.position, value.message)}>
                                                <HStack space={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <FontAwesome5 name='share' size={18} color={"#fff"} />
                                                </HStack>
                                            </TouchableOpacity>
                                        </HStack>
                                        <Divider my={2} backgroundColor='#272E48' />
                                        <Text style={{
                                            color: "#B5B5B2", fontSize: 10, marginTop: -3,
                                        }}>{moment(value.created_at).format("DD/MM/YY HH:MM")}</Text>
                                        <Text style={{ color: "#fff", marginTop: 5, fontSize: 15 }}>{value.message}</Text>
                                    </VStack>
                                </Box>
                                <Divider my={2} />
                            </View>
                        )) : <Text style={{ width: "99%", textAlign: 'center', color: "#fff", fontSize: 28 }}>Sem vagas no momento!</Text>}
                    </ScrollView>
                </View >
            </NativeBaseProvider >
        )
    }
}