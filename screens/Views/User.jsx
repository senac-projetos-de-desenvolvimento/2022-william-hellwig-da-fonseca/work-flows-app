import * as React from 'react';
import { View, Text, Image, } from 'react-native';
import {
    Box,
    NativeBaseProvider,
    Button,
    VStack,
    Toast,
    Modal,
    FormControl,
    WarningOutlineIcon,
    Input,
    HStack
} from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as ImagePicker from 'expo-image-picker';
import * as mime from 'react-native-mime-types';

import api from '../../services/api'
import Global from './../Components/Global';
import axios from 'axios';

export default class User extends React.Component {

    state = {
        userImg: Global.image,
        token: Global.token,
        image: null,
        imageR: null,
        idUser: Global.idUser,
        pass: "",
        newPass: "",
        confPass: "",
        erro: Boolean(false),
        erroMsg: "",
        baseURL: api.defaults.baseURL,
        showImg: false,
        showPass: Boolean
    }

    componentDidMount = async () => {
        let valueToken = await AsyncStorage.getItem('@workflows:token')
        this.setState({
            token: valueToken
        })
    }

    sendPass = async () => {
        const response = await api.post(`/userup/${this.state.token}`, {
            id: this.state.idUser,
            password: this.state.pass,
            newpassword: this.state.newPass
        })
        if (response.data.erro === false) {
            this.setState({
                showPass: false,
                pass: "",
                newPass: "",
                confPass: ""
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
        if (response.data.erro === true) {
            this.setState({
                showPass: false,
                pass: "",
                newPass: "",
                confPass: ""
            })
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }
    sendImage = async () => {
        const name = this.state.imageR.uri.replace(/^.*[\\\/]/, "")
        const url = `${this.state.baseURL}/altimg/${this.state.token}`
        const formData = new FormData();
        formData.append("id", this.state.idUser)
        formData.append("image", {
            name: name,
            type: mime.contentType(name),
            uri: this.state.imageR.uri,
        })
        // try {
        axios({
            url: url,
            method: 'POST',
            data: formData,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(function (response) {
                if (response.data) {
                    let dados = response.data
                    let erro = response.data.erro
                    alt(erro, dados)
                }
            })
            .catch(function (error) {
                console.log("error from image :", error);
            })

        const alt = (erro, dados) => {
            if (erro === false) {
                Global.image = dados.url
                this.setState({
                    showImg: false,
                    imageR: null,
                    image: null,
                    userImg: dados.url
                })
                Toast.show({
                    title: dados.msg,
                    placement: "top",
                    style: {
                        backgroundColor: "#354787",
                        fontSize: 20
                    }
                })
            }
            if (erro === true) {
                this.setState({
                    showImg: false,
                    imageR: null,
                    image: null
                })
                Toast.show({
                    title: dados.msg,
                    placement: "top",
                    style: {
                        backgroundColor: "#354787",
                        fontSize: 20
                    }
                })
            }
        }

    }
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [16, 16],
            quality: 1,
        });


        if (!result.cancelled) {
            this.setState({
                imageR: result,
                image: result.uri
            })
        }
    };

    handlePassChange = (pass) => {
        this.setState({ pass: pass })
    }
    handleNewPassChange = (newPass) => {
        this.setState({ newPass: newPass })
    }
    handleConfPassChange = (confPass) => {
        this.setState({ confPass: confPass })
    }

    render() {
        const { baseURL, userImg, pass, newPass, confPass, erroMsg, erro } = this.state

        const altPass = () => {
            if (newPass == confPass) {
                if (pass == newPass) {
                    this.setState({
                        erroMsg: " Nova Não Pode Ser Igual a Antiga!",
                        erro: true,
                        pass: "",
                        newPass: "",
                        confPass: ""
                    })
                } else {
                    this.sendPass();
                }
            } else {
                this.setState({
                    erroMsg: "Senhas Divergentes.",
                    erro: true,
                    newPass: "",
                    confPass: ""
                })
            }
        }
        return (
            <NativeBaseProvider>
                {/* Modal Imagem */}
                <Modal

                    isOpen={this.state.showImg}
                    onClose={() => this.setState({ showImg: false, image: null })}
                >

                    <Modal.Content style={{ width: "90%", justifyContent: 'center', alignItems: 'center' }}>
                        <Modal.CloseButton />
                        <Modal.Header>Atualizar Imagem</Modal.Header>
                        <Modal.Body style={{ justifyContent: 'center', alignItems: "center" }}>
                            <HStack>
                                {this.state.image && <Image source={{ uri: this.state.image }} style={{ width: 180, height: 180, borderRadius: 90 }} />}
                            </HStack>
                            <HStack style={{ marginTop: "2%" }}>
                                {this.state.image ?
                                    <Button style={{ width: "90%", backgroundColor: "#272E48", borderRadius:45 }} onPress={this.sendImage}>Enviar Imagem</Button>
                                    :
                                    <Button style={{ width: "90%", backgroundColor: "#272E48", borderRadius:45 }} onPress={this.pickImage}>Carregar Imagem</Button>
                                }
                            </HStack>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>

                {/* Modal Senha */}
                <Modal
                    isOpen={this.state.showPass}
                    onClose={() => this.setState({ showPass: false })}
                    size="xl"
                    style={{ backgroundColor: "#272E48" }}

                >
                    <Modal.Content >
                        <Modal.CloseButton />
                        <Modal.Header>Alteração De Senha</Modal.Header>
                        <Modal.Body>
                            <FormControl style={{ marginVertical: "2%" }}>
                                <Input require variant="outline" type='password' placeholder='Senha Antiga' value={this.state.pass} onChangeText={this.handlePassChange} />
                            </FormControl >
                            <FormControl isInvalid={erro} style={{ marginVertical: "2%" }}>
                                <Input require variant="outline" type='password' placeholder='Nova Senha' marginBottom="3%" value={this.state.newPass} onChangeText={this.handleNewPassChange} />
                                <Input require variant="outline" type='password' placeholder='Confirme Sua Senha' value={this.state.confPass} onChangeText={this.handleConfPassChange} />
                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                    {erroMsg}
                                </FormControl.ErrorMessage>
                            </FormControl>
                            <Button style={{ alignSelf: "center", width: "40%", height: "18%", margin: "5%", textAlign: "center", backgroundColor: "#272E48", borderRadius:45 }} onPress={() => altPass()}>
                                <Ionicons name='checkmark-outline' size={25} color={"#fff"} /></Button>
                        </Modal.Body>
                    </Modal.Content>
                </Modal>
                <View style={{
                    flex: 1,
                    resizeMode: "cover",
                    backgroundColor: "#272E48",
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Box safeAreaTop style={{
                        justifyContent: 'flex-start', width: "90%", height: "40%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 10,
                    }}>
                        <VStack style={{ width: "70%", height: "12%", alignItems: 'center', backgroundColor: "#272E48", borderRadius: 15, marginBottom: "5%" }}>
                            <Text style={{ width: "90%", height: "90%", fontSize: 17, textAlign: 'center', marginTop: "2%", color: "#fff" }}>Area Do Usuario</Text>
                        </VStack>
                        <VStack style={{ width: "38%", height: "45%", alignItems: 'center', justifyContent: 'center' }}>
                            <Image alt="Imagem do Ususario"
                                source={{ uri: baseURL + userImg }}
                                style={{ width: "99%", height: "99%", borderRadius: 90 }} />
                        </VStack>
                        <VStack style={{ alignItems: 'center', width: "50%", height: "15%", marginTop: "6%" }}>
                            <Button variant='solid' style={{ width: "90%", height: "90%", borderRadius: 25, backgroundColor: "#272E48" }} onPress={() => this.setState({ showImg: true })}>Alterar Imagem</Button>
                        </VStack>
                    </Box>
                    <Box safeAreaTop style={{
                        width: "90%", height: "20%", marginTop: "8%", alignItems: 'center', backgroundColor: "#354787", borderRadius: 10,
                    }}>
                        <VStack style={{ width: "70%", height: "27%", alignItems: 'center', backgroundColor: "#272E48", borderRadius: 15, marginBottom: "8%" }}>
                            <Text style={{ width: "90%", height: "90%", fontSize: 17, textAlign: 'center', marginTop: "2%", color: "#fff" }}>Alteração de Senha</Text>
                        </VStack>
                        <VStack style={{ alignItems: 'center', width: "50%", height: "33%" }}>
                            <Button variant='solid' style={{ width: "90%", height: "90%", borderRadius: 25, backgroundColor: "#272E48" }} onPress={() => this.setState({ showPass: true })}>Alterar Senha</Button>
                        </VStack>
                    </Box>
                </View>
            </NativeBaseProvider>
        );
    }
}