import * as React from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import Global from './Components/Global';

const CustomDrawer = (props) => {

    return (
        <View style={{
            flex: 1,
            resizeMode: "cover",
            backgroundColor: "#272E48"
        }}>
            <DrawerContentScrollView
                {...props}
                contentContainerStyle={{ backgroundColor: '#272E48' }}>
                <ImageBackground source={require('../assets/img/fundo.jpg')} style={{ padding: 20 }}>
                    <Image source={{ uri: Global.url + Global.image }} style={{ height: 80, width: 80, borderRadius: 40, marginBottom: 10 }} />
                    <Text style={{ color: "#fff", fontSize: 18, }}>{Global.name}</Text>
                    <Text style={{ color: "#ffff", }}>{Global.position}</Text>
                    <Text style={{ color: "#ffff", }}>{Global.score} Pontos</Text>
                </ImageBackground>
                <View style={{ flex: 1, backgroundColor: "#272E48", paddingTop: 10, marginBottom: "100%" }}>
                    <DrawerItemList {...props} />
                </View>
            </DrawerContentScrollView>
        </View>
    )
}

export default CustomDrawer