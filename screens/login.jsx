import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Box, Button, FormControl, HStack, Input, NativeBaseProvider, Toast, VStack } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../services/api'
import Global from './Components/Global';
import Helpers from './Components/Helpers';

export default function Login({ navigation }) {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [erro, setErro] = useState()
    const [erroMsg, setErroMsg] = useState()
    const [pushToken, setPushToken] = useState()

    useEffect(() => {
        Helpers.registerPushNotifications()
        getToken()
    }, []);

    const getToken = async () => {
        let user = await AsyncStorage.getItem('@workflows:login')
        let pass = await AsyncStorage.getItem('@workflows:password')
        let token = await AsyncStorage.getItem('@workflows:token')
        let pushToken = await AsyncStorage.getItem('@workflows:pushToken')
        setPushToken(pushToken)
        if (token && user && pass) {
            navigation.reset({
                index: 0,
                routes: [{ name: 'Index' }]
            })
        }
    }
    const login = async () => {
        const response = await api.post('/login', {
            email: email,
            password: password,
            pushToken: pushToken
        })
        if (response.data.erro === false) {

            Global.name = response.data.data.name
            Global.image = response.data.data.image
            Global.position = response.data.data.position
            Global.location = response.data.data.location
            Global.score = response.data.data.score
            Global.token = response.data.data.token
            Global.idUser = response.data.data.user_id
            Global.pushToken = pushToken

            await AsyncStorage.setItem('@workflows:token', String(response.data.data.token))
            await AsyncStorage.setItem('@workflows:idUser', String(response.data.data.user_id))
            await AsyncStorage.setItem('@workflows:login', String(email))
            await AsyncStorage.setItem('@workflows:password', String(password))

            navigation.reset({
                index: 0,
                routes: [{ name: 'Index' }]
            })

        } else {
            Toast.show({
                title: response.data.msg,
                placement: "top",
                style: {
                    backgroundColor: "#354787",
                    fontSize: 20
                }
            })
        }
    }
    const verify = () => {
        if (email == null || password == null) {
            setErro(true)
            setErroMsg("Informe os Dados de Login")
        } else {
            setErro(false)
            login()
        }
    }



    return (
        <NativeBaseProvider>
            <View style={{
                flex: 1,
                resizeMode: "cover",
                backgroundColor: "#272E48"
            }}>
                <VStack space={4} justifyContent="center" alignItems="center" style={{ height: "100%" }}>
                    <HStack>
                        <Image style={styles.tinyLogo} source={require('../assets/img/logo.png')} />
                    </HStack>
                    <VStack h="20%" w="90%" >
                        <FormControl isInvalid={erro}>
                            <Input
                                fontSize={18}
                                style={{ color: "#fff" }}
                                autoCapitalize="none"
                                variant='underlined'
                                placeholder="Email"
                                leftIcon={{ type: 'font-awesome', name: 'envelope' }}
                                onChangeText={value => setEmail(value)}
                                keyboardType='email-address'
                            // value={this.state.email}
                            />

                            <Input
                                fontSize={18}
                                style={{ color: "#fff" }}
                                secureTextEntry
                                leftIcon={{ type: 'font-awesome', name: 'key' }}
                                variant='underlined'
                                autoCapitalize="none"
                                placeholder="Senha"
                                onChangeText={value => setPassword(value)}
                            // value={this.state.password}
                            />
                            <FormControl.ErrorMessage>
                                {erroMsg}
                            </FormControl.ErrorMessage>
                        </FormControl>
                        <Box alignItems="center" marginTop={5}>
                            <Button h="12" w="90%" borderRadius={25} style={{ backgroundColor: "#354787" }} onPress={() => verify()}>Login</Button>
                        </Box>
                    </VStack>
                    <VStack w="90%" alignItems="center">
                    </VStack>
                </VStack>
            </View>
        </NativeBaseProvider>
    )
}

const styles = StyleSheet.create({
    logo: {
        paddingTop: '20%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputs: {
        margin: '2%',
        paddingTop: '15%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    tinyLogo: {
        width: "80%",
        height: 100,
        marginBottom: 22
    },
    btn: {
        margin: 10,
        borderRadius: 20,
        backgroundColor: '#354787',
    },
})


