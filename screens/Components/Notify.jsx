import React, { useState, useEffect, useRef } from 'react';
import { withNavigation } from 'react-navigation'
import * as Notifications from 'expo-notifications';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});

const Notify = ({ navigation }) => {
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    // Este ouvinte é acionado sempre que uma notificação é recebida enquanto o aplicativo está em primeiro plano.
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    // Este ouvinte é acionado sempre que um usuário toca ou interage com uma notificação (funciona quando o aplicativo está em primeiro plano, em segundo plano ou morto)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      navigation.reset({
        index: 0,
        routes: [{ name: 'Index' }]
      })
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);
}

export default withNavigation(Notify);
