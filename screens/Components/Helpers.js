import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Notifications from 'expo-notifications';
import Global from './Global';

const Helpers = {

    // Verifica se o usuário deu a permissão para as notificações e gera o token do dispositivo.
    registerPushNotifications: async () => {
        let token
        // Pede permissão do usuario.
        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;

        // Verifica se o usuário deu permissão.
        if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            alert('Falha ao obter o token push para notificação push!');
            return;
        }

        // Gera o token do dispositivo.
        token = (await Notifications.getExpoPushTokenAsync()).data;
        await AsyncStorage.setItem("@workflows:pushToken", token)
        // console.log(token);

        if (Platform.OS === 'android') {
            Notifications.setNotificationChannelAsync('default', {
                name: 'default',
                importance: Notifications.AndroidImportance.MAX,
                vibrationPattern: [0, 250, 250, 250],
                lightColor: '#FF231F7C',
            });
        }
        return token;
    }
}
export default Helpers; 0