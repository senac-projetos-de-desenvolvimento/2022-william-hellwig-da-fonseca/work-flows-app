import api from '../../services/api'

export default class Global {
    static name = "user"
    static position = "cargo"
    static image = "/images/User.png"
    static score = 0
    static token = ""
    static idUser = 0
    static url = api.defaults.baseURL
    static pushToken = ""
}