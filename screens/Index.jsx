import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';


import CustomDrawer from './CustomDrawer';

import Feed from './Views/Feed';
import User from './Views/User';
import Ponto from './Views/Ponto';
import Warning from './Views/Warning';
import Suggestions from './Views/Suggestion';
import Colabs from './Views/Colabs';
import Logout from './Views/Logout';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Global from './Components/Global';
import Ranking from './Views/Ranking';
import Vagas from './Views/Vagas';

const Drawer = createDrawerNavigator();

export default function Index() {
  return (
    // <NavigationContainer>
    <Drawer.Navigator drawerContent={props => <CustomDrawer {...props} />}
      screenOptions={{
        headerShown: false,
        drawerActiveBackgroundColor: "#354787",
        drawerActiveTintColor: "#fff",
        drawerInactiveTintColor: "#fff",
        drawerLabelStyle: { marginLeft: -25, fontSize: 15 }
      }}
      initialRouteName='Home'
    >

      {/* Feed */}
      <Drawer.Screen
        options={{
          drawerIcon: ({ color }) => (
            <Ionicons name='home-outline' size={22} color={color} />
          ),
          headerShown: false
        }}
        name="Home" component={Feed} />

      {/* Usuario */}
      <Drawer.Screen
        options={{
          drawerIcon: ({ color }) => (
            <Ionicons name='person-outline' size={22} color={color} />
          ),
          headerShown: false
        }}
        name="Perfil" component={User} />

      {/* Ponto */}
      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='time-outline' size={22} color={color} />
          ),
        }}
        name="Ponto" component={Ponto} />

      {/* Avisos */}
      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='warning-outline' size={22} color={color} />
          ),
        }}
        name="Avisos" component={Warning} />

      {/* Ideias */}
      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='bulb-outline' size={22} color={color} />
          ),
        }}
        name="Ideias" component={Suggestions} />

      {/* Vagas */}
      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='briefcase-outline' size={22} color={color} />
          ),
        }}
        name="Vagas" component={Vagas} />

      {/* Ranking */}
      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='podium-outline' size={22} color={color} />
          ),
        }}
        name="Ranking" component={Ranking} />

      {/* Colaboradores */}
      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='people-outline' size={22} color={color} />
          ),
        }}
        name="Colaboradores" component={Colabs} />

      <Drawer.Screen
        options={{
          headerShown: false,
          drawerIcon: ({ color }) => (
            <Ionicons name='log-out-outline' size={22} color={color} />
          ),
        }}
        name="Sair" component={Logout} />

    </Drawer.Navigator>
    // </NavigationContainer>
  );
}


