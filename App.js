import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import Login from './screens/login';
import Index from './screens/Index';
import Helpers from './screens/Components/Helpers';

Helpers.registerPushNotifications()

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}>
      <Stack.Screen
        name="Login"
        component={Login} />
      <Stack.Screen
        name="Index"
        component={Index} />

    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer >
      <MyStack />
    </NavigationContainer>
  )
}
