# WorkFlows - App :iphone:


## Descrição :page_facing_up:

Esta aplicação foi desenvolvida com o intuito de aplicar-se em toda e qualquer empresa, aquelas em que desejem manter sua estrutura “humana” engajada, motivada e próxima, que de forma eficiente irá trazer o crescimento, lucratividade e entrosamento da organização.

<!-- ## Capturas :camera_flash: -->


## Instalação do Projeto :construction_worker:

- Vamos iniciar com a Instalação do Node Js. [Obter Node js](https://nodejs.org/en/download/) 
  ````
   Node.js é um software que permite a execução de códigos JavaScript fora de um navegador web.
  ````
- O Proximo passo é instalar o Expo .
    ```
    O Expo é uma ferramenta de desenvolvimento de aplicativos mobile com React Native.
    ```
```
npm install --global expo-cli
```
- Com tudo devidamente instalado podemos clonar o repositorio.

```
git clone chave ssh. ou https.
```
- Agora entramos no diretorio do projeto e Intalamos as dependencias.
```
npm install
```
- Podemos agora executar a aplicação.
```
expo start
```

Sucesso :tada:


## Versão :bookmark:
- [Aplicativo V 1.0.0 TCC I](http://186.215.192.227:2606/apps/Work%20Flows%20V1.0.0%20TCC%20I.apk) :pushpin: 
- [Aplicativo V 2.0.0 TCC II](http://186.215.192.227:2606/apps/Work%20Flows%20V2.0.0%20TCC%20II.apk) :pushpin:


## Responsaveis :alien:

##### Desenvolvedor/Criador

- [Willian H. Da Fonseca](https://github.com/sakamottosann)
  
##### Orientador 

- [Angelo Luz](https://github.com/angelogluz)